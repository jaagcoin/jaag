#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR/..

DOCKER_IMAGE=${DOCKER_IMAGE:-jaagpay/jaagd-develop}
DOCKER_TAG=${DOCKER_TAG:-latest}

BUILD_DIR=${BUILD_DIR:-.}

rm docker/bin/*
mkdir docker/bin
cp $BUILD_DIR/src/jaagd docker/bin/
cp $BUILD_DIR/src/jaag-cli docker/bin/
cp $BUILD_DIR/src/jaag-tx docker/bin/
strip docker/bin/jaagd
strip docker/bin/jaag-cli
strip docker/bin/jaag-tx

docker build --pull -t $DOCKER_IMAGE:$DOCKER_TAG -f docker/Dockerfile docker
